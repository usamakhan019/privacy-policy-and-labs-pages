export const Theme = {
  colors: {
    primaryColor: '#018f9f',
    whiteColor: '#fff',
    orangeColor: '#ffa80a',
    blueColor: '#2db4eb',
    blackColor: '#262626',
    greyColor: '#5f5f5f',
  },
};
