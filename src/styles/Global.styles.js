import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`

html {
  font-size: 62.5%;
  scroll-behavior: smooth;
  box-sizing: border-box;

  
  body {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    *{
      font-family: Exo,
      sans-serif;
      margin: 0;
      padding: 0;
      box-sizing: border-box;
      list-style: none;
      text-decoration: none;
      color:unset;

      h1{
        font-size: 4rem;
        font-weight: bold;
      }

      p{
        font-size: 2rem;
        font-weight: normal;
      }
    }
  }
}
`;
