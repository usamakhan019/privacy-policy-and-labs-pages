import { css } from 'styled-components';

export const CustomContainerStyle = css`
  max-width: 117rem;
  margin: auto;
  padding: auto 1.5rem;
`;

export const FlexBoxStyle = css`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
`;

export const CommonGridStyle = css`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-template-rows: 1fr;
  justify-content: space-between;
  align-items: center;
`;
