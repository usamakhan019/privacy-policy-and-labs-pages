import React, { Component } from 'react';
import 'antd/dist/antd.css';

import { Labs } from './pages';
import { LabsCardsData } from './mockData';
import { GlobalStyle } from './styles/Global.styles';
import './App.css';

export default class App extends Component {
  render() {
    return (
      <>
        <GlobalStyle />
        <Labs labsCardsData={LabsCardsData} />
      </>
    );
  }
}
