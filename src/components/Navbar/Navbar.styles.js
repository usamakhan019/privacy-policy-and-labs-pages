import styled, { css } from 'styled-components';
import { CustomContainerStyle, FlexBoxStyle } from '../../styles/Common.styles';

export const CustomNavbarStyles = styled.section`
  .navbar-container {
    ${CustomContainerStyle}
    ${FlexBoxStyle}
    justify-content: space-between;

    .emumba-logo {
      width: 20rem;
      color: ${(props) => props.theme.colors.primaryColor};
    }
    
  }
  .test {
    text-transform: uppercase;
    font-weight: normal;
    color: ${(props) => props.theme.colors.greyColor};
    .ant-menu-item {
      &.ant-menu-item-active,
      &.ant-menu-item-selected {
        color: ${(props) => props.theme.colors.primaryColor};
        border-bottom: 2px solid ${(props) => props.theme.colors.primaryColor};
      }
    }
  }
`;
