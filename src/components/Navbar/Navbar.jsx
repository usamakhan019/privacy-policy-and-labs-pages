import React, { Component } from 'react';
import { Menu } from 'antd';
import {
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { EmumbaLogo } from '../../assets';
import { CustomNavbarStyles } from './Navbar.styles';

const { SubMenu } = Menu;
export default class Navbar extends Component {
  state = {
    current: 'mail',
  };

  handleClick = (e) => {
    console.log('click ', e);
    this.setState({ current: e.key });
  };

  render() {
    const { current } = this.state;
    return (
      <CustomNavbarStyles>
        <navbar className='navbar-container'>
          <figure className='emumba-logo'>
            <EmumbaLogo />
          </figure>
          <Menu
            onClick={this.handleClick}
            selectedKeys={[current]}
            mode='horizontal'
            className='test'
            triggerSubMenuAction='click'>
            <Menu.Item key='home'>home</Menu.Item>
            <SubMenu key='services' title='services'>
              <Menu.Item key='3'>Option 3</Menu.Item>
              <Menu.Item key='4'>Option 4</Menu.Item>
              <SubMenu key='sub1-2' title='Submenu'>
                <Menu.Item key='5'>Option 5</Menu.Item>
                <Menu.Item key='6'>Option 6</Menu.Item>
              </SubMenu>
            </SubMenu>
            <SubMenu key='solutions' title='solutions'>
              <Menu.Item key='3'>Option 3</Menu.Item>
              <Menu.Item key='4'>Option 4</Menu.Item>
              <SubMenu key='sub1-2' title='Submenu'>
                <Menu.Item key='5'>Option 5</Menu.Item>
                <Menu.Item key='6'>Option 6</Menu.Item>
              </SubMenu>
            </SubMenu>
            <Menu.Item key='customers'>customers</Menu.Item>
            <SubMenu key='company' title='company'>
              <Menu.Item key='3'>Option 3</Menu.Item>
              <Menu.Item key='4'>Option 4</Menu.Item>
              <SubMenu key='sub1-2' title='Submenu'>
                <Menu.Item key='5'>Option 5</Menu.Item>
                <Menu.Item key='6'>Option 6</Menu.Item>
              </SubMenu>
            </SubMenu>
          </Menu>
        </navbar>
      </CustomNavbarStyles>
    );
  }
}
