import React from 'react';
import classNames from 'classnames';
import { CardsWrapperStyle, LabsCardStyle } from './CardsSection.styles';

export default function CardsSection(props) {
  const { labsCardsData } = props;
  return (
    <CardsWrapperStyle>
      {labsCardsData.map((cardData, index) => (
        <LabsCardStyle key={index}>
          <figure
            className={classNames('labs-card-image', {
              'orange-image': cardData.color ,
            })}
            color={cardData.color}>
            {cardData.image}
          </figure>
          <figcaption>
            <h1 className='card-title'>{cardData.title}</h1>
            <p className='card-caption'>{cardData.caption}</p>
          </figcaption>
        </LabsCardStyle>
      ))}
    </CardsWrapperStyle>
  );
}
