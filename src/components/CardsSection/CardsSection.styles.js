import styled from 'styled-components';
import {
  CustomContainerStyle,
  CommonGridStyle,
} from '../../styles/Common.styles';

export const CardsWrapperStyle = styled.section`
  ${CustomContainerStyle}
  ${CommonGridStyle};

  grid-template-rows: calc(100% + );
  column-gap: 10rem;
  row-gap: 5rem;
  margin: 5rem auto;

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    justify-content: center;
  }

  .labs-card-image {
    width: 80%;
    margin: auto;
    color: ${(props) => props.theme.colors.blueColor};
  }
`;

export const LabsCardStyle = styled.a`
  position: relative;
  padding: 5rem;
  text-align: center;
  border-radius: 0.5rem;
  cursor: pointer;
  max-width: 50rem;

  @media (max-width: 768px) {
    margin: auto;
  }

  &::before {
    content: '';
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: -1;
    box-shadow: 0 0 3rem rgba(0, 0, 0, 0.1);
    transition: all 0.2s ease;
    border-radius: 0.5rem;
    position: absolute;
  }

  &:hover {
    color: ${(props) => props.theme.colors.blackColor};

    h1 {
      color: ${(props) => props.theme.colors.primaryColor};
    }
  }

  &:hover::before {
    transform: scale(1.1);
  }

  .orange-image {
    color: ${(props) => props.theme.colors.orangeColor};
  }

  .card-title {
    font-size: 1.8rem;
  }
  .card-caption {
    font-size: 1.4rem;
  }
`;
