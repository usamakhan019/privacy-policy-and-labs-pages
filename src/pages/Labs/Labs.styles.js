/* eslint-disable no-unused-vars */
import styled from 'styled-components';
import {
  CustomContainerStyle,
  CommonGridStyle,
} from '../../styles/Common.styles';

export const CustomHeaderStyle = styled.header`
  background: url('/images/overview.png');
  padding: 10rem 1.5rem;
  background-size: cover;
  background-position: center;
  text-align: center;
  width: 100%;

  .banner-container {
    ${CustomContainerStyle}
    color: ${(props) => props.theme.colors.whiteColor};
  }
`;

