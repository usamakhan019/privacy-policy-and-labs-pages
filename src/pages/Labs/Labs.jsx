import React from 'react';

import { CardsSection, Navbar } from '../../components';
import { CustomHeaderStyle } from './Labs.styles';

const LabHeader = () => (
  <CustomHeaderStyle>
    <article className='banner-container'>
      <h1 className='banner-title'>Labs</h1>
      <p className='banner-description'>
        It is imperative to stay at the cutting edge of innovation. The purpose
        of these labs is to encourage disruption and experimentation in various
        technologies listed below and more. Ideas born in our labs eventually
        mature into our core service or solution.
      </p>
    </article>
  </CustomHeaderStyle>
);

export default function Labs(props) {
  const { labsCardsData } = props;
  return (
    <React.Fragment>
      {/* <Navbar /> */}
      <LabHeader />
      <CardsSection labsCardsData={labsCardsData} />
    </React.Fragment>
  );
}
